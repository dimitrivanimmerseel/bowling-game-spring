package be.thebeehive.kata.api.controller.bowlinggame;

import be.thebeehive.kata.api.dto.ExceptionResponse;
import be.thebeehive.kata.api.exceptions.GameIsOverException;
import be.thebeehive.kata.api.exceptions.InvalidGameIdException;
import be.thebeehive.kata.api.exceptions.InvalidRollException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(basePackages=  "be.thebeehive.kata.api.controller.bowlinggame")
public class BowlingGameControllerAdvice {
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleMessageNotReadableException(HttpMessageNotReadableException exception) {
        return new ExceptionResponse("Invalid request. Verify all required data is included");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException exception){
        return  new ExceptionResponse("name:\tName is required");
    }



    @ExceptionHandler(InvalidGameIdException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleInvalidGameIdException(InvalidGameIdException exception){
        return  new ExceptionResponse(String.format("Game with id %s not found", exception.getMessage()));
    }

    @ExceptionHandler(InvalidRollException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleInvalidScoreException(InvalidRollException exception){
        return  new ExceptionResponse("pins:\tInvalid number of pins");
    }

    @ExceptionHandler(GameIsOverException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleGameIsOverException(GameIsOverException exception) {
        return new ExceptionResponse(String.format("Game over. Final score is %s",exception.getMessage()));
    }
}