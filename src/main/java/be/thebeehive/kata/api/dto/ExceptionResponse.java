package be.thebeehive.kata.api.dto;

public record ExceptionResponse(String message) {
}
