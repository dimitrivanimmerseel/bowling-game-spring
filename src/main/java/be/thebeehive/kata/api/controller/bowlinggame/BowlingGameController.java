package be.thebeehive.kata.api.controller.bowlinggame;

import be.thebeehive.kata.api.configuration.UserDetailsImpl;
import be.thebeehive.kata.api.dto.BowlingGameDto;
import be.thebeehive.kata.api.dto.CreateGameDto;
import be.thebeehive.kata.api.dto.RollDto;
import be.thebeehive.kata.api.exceptions.InvalidGameIdException;
import be.thebeehive.kata.api.services.BowlingGameService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController //Maps response to json/xml
@RequiredArgsConstructor //All final fields are required
@RequestMapping("/bowling")
@CrossOrigin
class BowlingGameController {

    private final BowlingGameService bowlingGameService;

    //Valid for constraints violations, requestbody for data in POST body
    @PostMapping
    private BowlingGameDto createNewGame(@Valid @RequestBody CreateGameDto createGameDto,@AuthenticationPrincipal UserDetailsImpl userDetails){
        return bowlingGameService.createGame(createGameDto, userDetails.getUser());
    }

    @PostMapping("/{id}")
    public BowlingGameDto saveRegularScoring(@Valid @RequestBody RollDto rollDto, @PathVariable(value="id") String gameId) throws InvalidGameIdException {
        //return bowlingGameService.saveScoring(rollDto, gameId);
        BowlingGameDto dto = bowlingGameService.saveRoll(rollDto, gameId);
        return dto;
    }



}
