package be.thebeehive.kata.api.configurations;

import be.thebeehive.kata.api.configuration.UserDetailsImpl;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.util.BaseIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@TestConfiguration
@Log4j2
public class SpringSecurityTestConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        User basicUser = BaseIntegrationTest.getUniqueDummyUser();
        UserDetailsImpl basicUserDetails = new UserDetailsImpl(basicUser);
        return new InMemoryUserDetailsManager(basicUserDetails);
    }

    @Bean
    @Primary
    public PasswordEncoder dummypasswordEncoder() {
        return NoOpPasswordEncoder.getInstance() ;
    }



}