package be.thebeehive.kata.api.controller;


import be.thebeehive.kata.api.dto.SignupDto;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD) //I want a new H2 for every test
public class CreateUserIT extends BaseIntegrationTest {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Test
    void createUserWithValidSignupDto() throws Exception {
        SignupDto newUser = new SignupDto("dummy","dummy@dummy.dummy","Passwords10_");
        MockHttpServletResponse response = mockMvc.perform(
                        post("/signup")
                                .content(serialize(newUser))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andReturn()
                .getResponse();

        SignupDto result = deserialize(response.getContentAsString(), SignupDto.class);
        assertThat(result)
                .usingRecursiveComparison()
                .ignoringFields("password")
                .isEqualTo(newUser);
    }

    @Test
    void createUserWithValidSignupShouldEncryptPassword() throws Exception {
        SignupDto newUser = new SignupDto("dummy","dummy@dummy.dummy","Passwords10_");
        MockHttpServletResponse response = mockMvc.perform(
                        post("/signup")
                                .content(serialize(newUser))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andReturn()
                .getResponse();
        String encrypted = passwordEncoder.encode(newUser.password());
        SignupDto result = deserialize(response.getContentAsString(), SignupDto.class);
        assertThat(result.password()).isEqualTo(encrypted);
    }

    @Test
    @Transactional
    void createUserWithExistingEmailShouldThrowEmailExistsException() throws Exception{
        User dummy = getUniqueDummyUser();
        entityManager.persist(dummy);
        SignupDto newUser = new SignupDto(dummy.getName(),dummy.getEmail(), dummy.getPassword());
        mockMvc.perform(
                        post("/signup")
                                .content(serialize(newUser))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("This email address is already registered."));
    }

    @Test
    void createUserWithEmptyBodyShouldReturnBadRequest() throws Exception{
        mockMvc.perform(
                        post("/signup")
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Invalid request. Verify all required data is included"));
    }

    @Test
    void createUserWithInvalidEmailShouldReturnConflict() throws Exception{
        SignupDto newUser = new SignupDto("dummy","emailaddress","Password10_");
        mockMvc.perform(
                        post("/signup")
                                .content(serialize(newUser))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isConflict())
                .andExpect(jsonPath("$.message").value("Constraint violation."));
    }

    @ParameterizedTest
    @ValueSource(strings = {" ", "\t", "\n"})
    @NullAndEmptySource
    void createUserWithEmptyNameShouldReturnConflict(String strings) throws Exception{
        SignupDto newUser = new SignupDto(strings,"dummy@dummy.dummy","Password10_");
        mockMvc.perform(
                        post("/signup")
                                .content(serialize(newUser))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isConflict())
                .andExpect(jsonPath("$.message").value("Constraint violation."));
    }

    @ParameterizedTest
    @ValueSource(strings = {"p", "pa", "pas","pass","passw","password","Password"})
    @NullAndEmptySource
    void createUserWithInvalidPasswordShouldReturnConflict(String pass) throws Exception{
        SignupDto newUser = new SignupDto("dummy","dummy@dummy.dummy",pass);
        mockMvc.perform(
                        post("/signup")
                                .content(serialize(newUser))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isConflict())
                .andExpect(jsonPath("$.message").value("Constraint violation."));
    }





}
