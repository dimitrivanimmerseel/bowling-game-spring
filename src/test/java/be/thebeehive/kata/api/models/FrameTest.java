package be.thebeehive.kata.api.models;

import be.thebeehive.kata.api.exceptions.InvalidRollException;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@Log4j2
class FrameTest {

    private Frame frame;
    @BeforeEach
    void initEach(){
        frame = new Frame();
    }

    @ParameterizedTest
    @ValueSource(ints = {0,1,2,3,4,5,6,7,8,9,10})
    void addRollShouldAddValidSingleRollToFrame(int pins ){
        //given | arrange
        Roll newRoll = new Roll(pins);
        //when | act
        frame.addRoll(newRoll);
        //then | assert
        assertEquals(pins,frame.getScore());
    }
    @ParameterizedTest
    @ValueSource(ints = {0,1,2,3,4,5,6,7,8,9})
    void addRollShouldAddValidDoubleRollToFrame(int pins ){
        Roll firstRoll = new Roll(pins);
        Roll secondRoll = new Roll(10-pins);
        frame.addRoll(firstRoll);
        frame.addRoll(secondRoll);
        assertEquals(10, frame.getScore());
    }

    @Test
    void shouldThrowExceptionIfSumOfRollsIsLargerThanTen(){
        Roll firstRoll = new Roll(9);
        frame.addRoll(firstRoll);
        Roll secondRoll = new Roll(2);
        assertThrows(InvalidRollException.class,()->{frame.addRoll(secondRoll);});
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6,7,8,9})
    void isSpareShouldReturnTrueIfSumOfRollsIsTen(int pins) {
        Roll firstRoll = new Roll(pins);
        Roll secondRoll = new Roll(10-pins);
        frame.addRoll(firstRoll);
        frame.addRoll(secondRoll);
        assertTrue(frame.isSpare());
    }
    @Test
    void isSpareShouldReturnTrueIfSecondRollIsStrikeAndFirstZero(){
        Roll first = new Roll(0);
        Roll second = new Roll(10);
        frame.addRoll(first);
        frame.addRoll(second);
        assertTrue(frame.isSpare());
    }

    @Test
    void isSpareShouldReturnFalseIfFirstRollIsStrike(){
        Roll strike = new Roll(10);
        frame.addRoll(strike);
        assertFalse(frame.isSpare());
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,})
    void getScoreShouldReturnSumOfFrameIfNoStrikeOrSpareWasThrown(int given) {
        Roll first = new Roll(given);
        Roll second = new Roll(given);
        frame.addRoll(first);
        frame.addRoll(second);
        assertEquals((given+given),frame.getScore());
    }
    @Test
    void getScoreShouldCountNextRollIfSpareIsThrown(){
        final int NEXT_FRAME_PINS= 1;
        final int FRAME_SCORE = 5;
        Frame nextFrame = new Frame();
        nextFrame.addRoll(new Roll(NEXT_FRAME_PINS));
        frame.setNextFrame(nextFrame);
        Roll first = new Roll(5);
        Roll second = new Roll(5);
        frame.addRoll(first);
        frame.addRoll(second);
        assertEquals(frame.getScore(),(FRAME_SCORE+FRAME_SCORE+NEXT_FRAME_PINS) );
    }

    @Test
    void getScoreShouldCountNextTwoRollsIfStrikeIsThrown(){
        final int STRIKE_SCORE = 10;
        final int NEXT_FRAME_PINS= 1;
        Frame nextFrame = new Frame();
        frame.setNextFrame(nextFrame);
        nextFrame.addRoll(new Roll(NEXT_FRAME_PINS));
        nextFrame.addRoll(new Roll(NEXT_FRAME_PINS));
        frame.addRoll(new Roll(STRIKE_SCORE));
        assertEquals(frame.getScore(),(STRIKE_SCORE+NEXT_FRAME_PINS+NEXT_FRAME_PINS) );
    }

    @Test
    void getScoreShouldCountNextTwoFramesIfDoubleStrikeIsThrown(){
        final int STRIKE_SCORE = 10;
        Frame strikeFrame = new Frame();
        Frame nextStrikeFrame = new Frame();
        strikeFrame.addRoll(new Roll(STRIKE_SCORE));
        nextStrikeFrame.addRoll(new Roll(STRIKE_SCORE));
        strikeFrame.setNextFrame(nextStrikeFrame);
        frame.setNextFrame(strikeFrame);
        frame.addRoll(new Roll(STRIKE_SCORE));
        assertEquals(30,frame.getScore());
    }

    @Test
    void lastFrameShouldSaveThirdRollIfSpareIsThrown(){
        Frame lastFrame = new Frame(true);
        Roll zeroRoll = new Roll(0);
        Roll strikeRoll = new Roll(10);
        Roll thirdRoll = new Roll(1);
        int expected = zeroRoll.getPins()+strikeRoll.getPins()+thirdRoll.getPins();

        lastFrame.addRoll(zeroRoll);
        lastFrame.addRoll(strikeRoll);
        lastFrame.addRoll(thirdRoll);
        assertEquals(expected,lastFrame.getScore());
    }
    @Test
    void lastFrameShouldSaveThirdRollIfDoubleStrikeIsThrown(){
        Frame lastFrame = new Frame(true);
        Roll strikeRoll = new Roll(10);
        Roll thirdRoll = new Roll(1);
        int expected = (strikeRoll.getPins()+strikeRoll.getPins()+thirdRoll.getPins())+ strikeRoll.getPins()+thirdRoll.getPins();
        lastFrame.addRoll(strikeRoll);
        lastFrame.addRoll(strikeRoll);
        lastFrame.addRoll(thirdRoll);
        assertEquals(expected,lastFrame.getScore());
    }
}