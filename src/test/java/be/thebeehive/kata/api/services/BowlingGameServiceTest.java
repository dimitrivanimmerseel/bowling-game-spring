package be.thebeehive.kata.api.services;

import be.thebeehive.kata.api.exceptions.InvalidGameIdException;
import be.thebeehive.kata.api.models.BowlingGame;
import be.thebeehive.kata.api.repositories.GameRepository;
import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;


class BowlingGameServiceTest extends BaseIntegrationTest {
    @InjectMocks
    private  BowlingGameService gameService;

    @Mock
    private GameRepository repository;

    @Before
    void init(){
        MockitoAnnotations.initMocks(this);
        gameService = new BowlingGameService(repository);
    }
    @Test
    void shouldReturnGameIfValidGameIdIsGiven(){
        String uuid = UUID.randomUUID().toString();
        BowlingGame dummyGame = new BowlingGame("dummy");
        dummyGame.setGameId(uuid);
        doReturn(dummyGame).when(repository).getBowlingGameByGameId(uuid);
        assertEquals(uuid, gameService.getBowlingGameByGameId(uuid).getGameId());
    }

    @Test
    void shouldThrowInvalidGameExceptionIfInvalidGameIdIsGiven(){
        String gameId="NOT A SAVED UUID";
        assertThrows(InvalidGameIdException.class,()->gameService.getBowlingGameByGameId(gameId));
    }


}