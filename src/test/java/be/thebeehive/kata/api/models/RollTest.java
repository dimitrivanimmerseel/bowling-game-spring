package be.thebeehive.kata.api.models;

import be.thebeehive.kata.api.exceptions.InvalidRollException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class RollTest {

    @ParameterizedTest
    @ValueSource(ints = {-1,11})
    void invalidRollShouldThrowInvalidRollException(int pins){
        assertThrows(InvalidRollException.class,()->{ new Roll(pins);});
    }

    @Test
    void isStrikeShouldReturnTrueIfPinsisTen(){
        final int STRIKE_PINS=10;
        Roll newRoll = new Roll(STRIKE_PINS);
        assertTrue(newRoll.isStrike());
    }
    @ParameterizedTest
    @ValueSource(ints = {0, 1,2,3,4,5,6,7,8,9})
    void isStrikeShouldBeFalseIfPinsIsSmallerThanTen(int pins){
        Roll newRoll = new Roll(pins);
        assertFalse(newRoll.isStrike());
    }

}