package be.thebeehive.kata.api.models;

import be.thebeehive.kata.api.exceptions.InvalidRollException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@Entity
@NoArgsConstructor
public class Roll {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "frame_id", nullable = false)
    @Setter
    private Frame frame;
    @Getter
    private int pins;
    public Roll(int pins) {
        if (pins<0 || pins>10){
            throw new InvalidRollException();
        }
        this.pins = pins;
    }

    public boolean isStrike() {
        return pins==10;
    }

}