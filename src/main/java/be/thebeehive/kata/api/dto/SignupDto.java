package be.thebeehive.kata.api.dto;

import lombok.Builder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Builder
public record SignupDto(
        @NotBlank
        String name,
        @Email
        String email,
        @NotBlank
        @Pattern(regexp = "^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z]).*$")
        String password
) {
}
