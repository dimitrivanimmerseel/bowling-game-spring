package be.thebeehive.kata.api.dto;

import lombok.Builder;

@Builder
public record BowlingGameDto(
        String gameId,
        String name,
        int score
) {
}
