package be.thebeehive.kata.api.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public record LoginDto(
        @Email
        String email,
        @NotBlank
        @Pattern(regexp = "^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z]).*$")
        String password
) {
}
