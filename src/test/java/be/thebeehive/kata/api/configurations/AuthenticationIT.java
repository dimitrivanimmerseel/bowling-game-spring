package be.thebeehive.kata.api.configurations;

import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthenticationIT extends BaseIntegrationTest {

    @Test
    public void unauthorizedRequestShouldReturnUnauthorized() throws Exception {
        mockMvc.perform(
                post("/bowling")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("dummy@dummy.be")
    public void authorizedRequestShouldNotReturnForbidden() throws Exception {
        mockMvc.perform(
                post("/bowling")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/signin","/signup"})
    public void signupAndSignInShouldNotReturnForbidden(String endpoint) throws Exception{
        mockMvc.perform(
                post(endpoint)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }





}
