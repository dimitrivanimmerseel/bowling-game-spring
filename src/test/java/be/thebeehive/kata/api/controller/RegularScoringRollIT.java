package be.thebeehive.kata.api.controller;

import be.thebeehive.kata.api.dto.RollDto;
import be.thebeehive.kata.util.BaseIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@AutoConfigureTestEntityManager
@Log4j2
class RegularScoringRollIT extends BaseIntegrationTest {
    private String gameId;

    @BeforeEach
    @Transactional
    void setUp() {
        gameId = setupGame();
        log.error(gameId);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
    @Transactional
    void singleRollAddsResultToScore(int pins) throws Exception {
        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(pins)))
                ).andExpect(status().isOk())
                .andExpect(jsonPath("$.score").value(pins));
    }

    @Test
    @Transactional
    void consecutiveRollsGiveExpectedTotalScore() {
        int[] rolls = {0,4,6,2,7,2};
        int expectedScore = Arrays.stream(rolls).sum();

        var gameDto = consecutiveRolls(gameId, rolls);

        assertThat(gameDto.score()).isEqualTo(expectedScore);
    }
}
