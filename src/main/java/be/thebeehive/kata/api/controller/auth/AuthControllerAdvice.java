package be.thebeehive.kata.api.controller.auth;

import be.thebeehive.kata.api.dto.ExceptionResponse;
import be.thebeehive.kata.api.exceptions.EmailExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(basePackages = "be.thebeehive.kata.api.controller.auth")
public class AuthControllerAdvice {


    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleMessageNotReadableException(HttpMessageNotReadableException exception) {
        return new ExceptionResponse("Invalid request. Verify all required data is included");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ExceptionResponse handleConstraintViolationException(){
        return new ExceptionResponse("Constraint violation.");
    }

    @ExceptionHandler(EmailExistsException.class)
    @ResponseStatus(HttpStatus.OK)  //Nothing is wrong with the request
    public ExceptionResponse handleEmailExistsException(){
        return new ExceptionResponse("This email address is already registered.");
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ExceptionResponse handleBadCredentialsException(){
        return new ExceptionResponse("Unauthorized. Bad credentials");
    }
}
