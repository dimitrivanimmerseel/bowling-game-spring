package be.thebeehive.kata.api.models;


import be.thebeehive.kata.api.exceptions.GameIsOverException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Getter @Setter
@Entity
@NoArgsConstructor
public class BowlingGame {

    private String name;
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator="uuid")
    private String gameId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Transient
    private final int MAX_FRAMES=10;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "game")
    private List<Frame> frameList;

    private boolean finished;


    public BowlingGame(String name) {
        this.name = name;
        this.frameList = new ArrayList<>();
        this.finished=false;
    }

    public int getScore(){
        int total = 0;
        for (Frame frame : this.frameList) {
            total+=frame.getScore();
        }
        return total;
    }

    public void addScore(Roll roll) {
        int numFrames = frameList.size();
        if (numFrames==0){
            Frame newFrame = new Frame();
            newFrame.setGame(this);
            newFrame.addRoll(roll);
            frameList.add(newFrame);
        } else if (numFrames<10) {
            Frame current = frameList.get(numFrames-1);
            if (current.getRolls().size()<2 && !current.getRolls().get(0).isStrike()){
                    current.addRoll(roll);
            }else{
                Frame newFrame = new Frame();
                newFrame.setGame(this);
                newFrame.addRoll(roll);
                frameList.get(numFrames-1).setNextFrame(newFrame);
                frameList.add(newFrame);
                if (frameList.size()==10){
                    newFrame.setLastFrame(true);
                }
            }
        } else if (numFrames==10) {
             Frame current = frameList.get(numFrames-1);
             if (current.getRolls().size()<2){
                 current.addRoll(roll);
             }else if ((current.isSpare() || current.getRolls().get(0).isStrike()) && current.getRolls().size()<3){
                current.addRoll(roll);
                this.finished=true;
             }else{
                 throw new GameIsOverException(getScore());
             }
        }
    }

}
