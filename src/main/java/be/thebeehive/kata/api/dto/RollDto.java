package be.thebeehive.kata.api.dto;

import javax.validation.constraints.NotNull;

public record RollDto(
        @NotNull
        int pins
) {
}
