package be.thebeehive.kata.api.configuration;

import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.api.repositories.UserRepository;
import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserDetailsServiceImplTest extends BaseIntegrationTest {

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;


    @Test
    void loadUserByUsernameWithValidEmailShouldReturnUser() {
        User dummy = getUniqueDummyUser();
        userDetailsService = new UserDetailsServiceImpl(userRepository);
        when(userRepository.findByEmail(dummy.getEmail())).thenReturn(dummy);
        userDetailsService.loadUserByUsername(dummy.getEmail());
        verify(userRepository).findByEmail(dummy.getEmail());
    }

    @Test
    void loadUserByUsernameWithInvalidEmailShouldReturnNull(){
        User dummy = getUniqueDummyUser();
        userDetailsService = new UserDetailsServiceImpl(userRepository);
        when(userRepository.findByEmail(dummy.getEmail())).thenReturn(null);
        assertThrows(UsernameNotFoundException.class,()-> userDetailsService.loadUserByUsername(dummy.getEmail()));
        verify(userRepository).findByEmail(dummy.getEmail());
    }
}