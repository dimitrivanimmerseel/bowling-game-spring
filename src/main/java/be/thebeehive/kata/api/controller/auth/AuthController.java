package be.thebeehive.kata.api.controller.auth;


import be.thebeehive.kata.api.dto.LoginDto;
import be.thebeehive.kata.api.dto.SignupDto;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.api.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Log4j2
@RequiredArgsConstructor
@CrossOrigin
public class AuthController {
    private final UserService userService;
    private final AuthenticationProvider authenticationProvider;

    private final PasswordEncoder bcryptPasswordEncoder;

    @PostMapping("/signup")
    public SignupDto registerUser(@Valid @RequestBody SignupDto registerDto){
        // create user object
        User user = User.builder()
                        .name(registerDto.name())
                        .email(registerDto.email())
                        .password(bcryptPasswordEncoder.encode(registerDto.password()))
                        .build();
        user = userService.createUser(user);

        return SignupDto.builder()
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }

    @PostMapping("/signin")
    public LoginDto loginUser(@Valid @RequestBody LoginDto loginDto){
        Authentication authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(
                loginDto.email(), loginDto.password()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return loginDto;
    }
}