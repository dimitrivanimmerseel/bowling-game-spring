package be.thebeehive.kata.api.repositories;

import be.thebeehive.kata.api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

    boolean existsByEmail(String email);
    User findByEmail(String email);
    User save(User user);


}
