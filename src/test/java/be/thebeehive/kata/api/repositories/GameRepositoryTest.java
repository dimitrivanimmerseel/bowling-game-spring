package be.thebeehive.kata.api.repositories;

import be.thebeehive.kata.api.models.BowlingGame;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.util.BaseIntegrationTest;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest //H2 config, hibernate, spring data, datasource, entityscan, sql log
@Log4j2
@AutoConfigureTestEntityManager
class GameRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private GameRepository gameRepository;

    @Test
    void saveShouldSaveAndReturnGameWithId() {
        BowlingGame given = new BowlingGame("name");
        User user = new User("dummy","dummy@dummy.be","encrypted");
        given.setUser(user);
        BowlingGame savedGame = gameRepository.save(given);
        assertThat(savedGame.getName()).isEqualTo(given.getName());
        assertThat(savedGame.getGameId()).isEqualTo(given.getGameId());
        assertThat(savedGame.getScore()).isEqualTo(given.getScore());
    }

    @Test
    void getBowlingGameByGameIdShouldReturnSavedBowlingGame() {
        BowlingGame given = new BowlingGame("name");
        entityManager.persist(given);
        entityManager.flush();
        BowlingGame found = gameRepository.getBowlingGameByGameId(given.getGameId());

        assertThat(found.getGameId()).isEqualTo(given.getGameId());

    }

    @Test
    void existsBowlingGameByGameId() {
        BowlingGame given = new BowlingGame("name");
        entityManager.persist(given);
        entityManager.flush();
        assertThat(gameRepository.existsBowlingGameByGameId(given.getGameId())).isTrue();
    }

    @Test
    void getBowlingGameByUser(){
        BowlingGame given = new BowlingGame("name");
        User dummy = BaseIntegrationTest.getUniqueDummyUser();
        given.setUser(dummy);
        entityManager.persist(dummy);
        entityManager.persist(given);
        entityManager.flush();
        List<BowlingGame> found = gameRepository.getBowlingGamesByUser(dummy);
        assertThat(found.size()).isEqualTo(1);
    }
}