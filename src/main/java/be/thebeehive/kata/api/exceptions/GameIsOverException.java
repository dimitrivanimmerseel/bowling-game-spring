package be.thebeehive.kata.api.exceptions;

public class GameIsOverException extends RuntimeException{

    public GameIsOverException(int score) {
        super(String.valueOf(score));
    }
}
