package be.thebeehive.kata.api.models;

import be.thebeehive.kata.api.exceptions.GameIsOverException;
import be.thebeehive.kata.api.exceptions.InvalidRollException;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
@Log4j2
class BowlingGameTest {

    private BowlingGame game;

    @BeforeEach
    void init(){
        game = new BowlingGame("MyBowlinggame");
    }

    @Test
    void getScoreShouldReturnSumOfFrameScores() {
        game.addScore(new Roll(10));
        assertEquals(game.getScore(),game.getFrameList().stream().mapToInt(Frame::getScore).sum());
    }

    @Test
    void finishedShouldBeFalseWhenGameIsCreated(){
        assertFalse(game.isFinished());
    }


    @Test
    void addScoreShouldThrowExceptionIfRollIsInvalid() {
        //I test this in roll unit test
        assertThrows(InvalidRollException.class, ()-> game.addScore(new Roll(11)));
        assertThrows(InvalidRollException.class, ()-> game.addScore(new Roll(-1)));
    }
    @Test
    void shouldAddLastFrameAfterNineFrames(){
        for (int i = 1; i <=10 ; i++) {
            game.addScore(new Roll(10));
        }
        assertTrue(game.getFrameList().get(game.getFrameList().size()-1).isLastFrame());
    }

    @Test
    void shouldThrowExceptionWhenMaxFramesIsExceeded(){
        for (int i = 0; i <=11 ; i++) {
            game.addScore(new Roll(10));
        }
        assertThrows(GameIsOverException.class,()->game.addScore(new Roll(1)));
    }
}