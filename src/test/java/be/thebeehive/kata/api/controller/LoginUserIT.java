package be.thebeehive.kata.api.controller;

import be.thebeehive.kata.api.dto.LoginDto;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LoginUserIT extends BaseIntegrationTest {
    @Test
    public void shouldLoginValidUser() throws Exception{
        User dummy = getUniqueDummyUser();
        LoginDto loginDto = new LoginDto(dummy.getEmail(), dummy.getPassword());
        MockHttpServletResponse response = mockMvc.perform(
                        post("/signin")
                                .content(serialize(loginDto))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andReturn()
                .getResponse();
        LoginDto result = deserialize(response.getContentAsString(), LoginDto.class);
        assertThat(result)
                .usingRecursiveComparison()
                .isEqualTo(loginDto);
    }

    @Test
    public void shouldNotLoginInvalidUser() throws Exception{
        LoginDto loginDto = new LoginDto("invalid@email.be", "LegitPassword10");
        mockMvc.perform(
                        post("/signin")
                                .content(serialize(loginDto))
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.message").value("Unauthorized. Bad credentials"));
    }
}
