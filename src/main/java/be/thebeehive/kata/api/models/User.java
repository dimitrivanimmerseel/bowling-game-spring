package be.thebeehive.kata.api.models;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity(name = "users")
@NoArgsConstructor
@Getter @Setter
@Builder
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    private String name;
    @Email
    private String email;
    @NotBlank
    @Length(min=10) //Cant do regex here, encoded password never matches
    private String password;
    @OneToMany (cascade = CascadeType.ALL, mappedBy = "user")
    private List<BowlingGame> gameList;


    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }
}