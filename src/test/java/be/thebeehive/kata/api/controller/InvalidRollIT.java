package be.thebeehive.kata.api.controller;

/**
 * Uncomment the comments when you start working on this
 */

import be.thebeehive.kata.api.dto.RollDto;
import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class InvalidRollIT extends BaseIntegrationTest {
    public static final String GAME_FIELD = "game";

    private String gameId;

//    @Autowired
//    BowlingService service;

    @BeforeEach
    void setUp() {
        gameId = setupGame();
    }

    @Test
    @Transactional
    void rollWithUnexistingGameIdReturnsExpectedWhenNoGameExists() throws Exception {
//        ReflectionUtil.clearField(service, GAME_FIELD);
        gameId = "u-".concat(UUID.randomUUID().toString());
        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(5)))
                ).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value(String.format("Game with id %s not found", gameId)));
    }

    @Test
    @Transactional
    void rollWithUnexistingGameIdReturnsExpectedWhenGameExists() throws Exception {
        gameId = "u-".concat(UUID.randomUUID().toString());
        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(5)))
                ).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value(String.format("Game with id %s not found", gameId)));
    }

    @Test
    @Transactional
    void rollWithNegativeNumberOfPinsReturnsExpectedException() throws Exception {
        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(-5)))
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("pins:\tInvalid number of pins"));
    }

    @Test
    @Transactional
    void rollWithExcessiveNumberOfPinsReturnsExpectedException() throws Exception {
        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(11)))
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("pins:\tInvalid number of pins"));
    }

    @Test
    @Transactional
    void zeroGameStopsAfterFrame10() throws Exception {
        consecutiveRolls(gameId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(9)))
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Game over. Final score is 0"));
    }

    @Test
    @Transactional
    void perfectGameStopsAt300() throws Exception {
        consecutiveRolls(gameId, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

        mockMvc.perform(
                        post(String.format("/bowling/%s", gameId))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(new RollDto(9)))
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Game over. Final score is 300"));
    }
}
