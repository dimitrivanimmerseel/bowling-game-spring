package be.thebeehive.kata.api.models;


import be.thebeehive.kata.api.exceptions.InvalidRollException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

@Getter
@Setter
@Entity
public class Frame {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "frame")
    private List<Roll> rolls;
    @ManyToOne
    @JoinColumn(name="game_id", nullable=false)
    private BowlingGame game;
    @OneToOne(cascade = CascadeType.ALL )
    private Frame nextFrame;
    private boolean lastFrame;

    public Frame() {
        this.rolls = new ArrayList<>();
        nextFrame = null;
        this.lastFrame=false;
    }
    public Frame(boolean last){
        this();
        this.lastFrame=last;
    }

    public boolean isSpare(){
        if (rolls.size()>1) {
            return !rolls.get(0).isStrike() && rolls.get(0).getPins() + rolls.get(1).getPins() == 10;
        }else{
            return false;
        }
    }

    public void addRoll(Roll roll){
        roll.setFrame(this);
        if (this.getRolls().stream().mapToInt(Roll::getPins).sum()+roll.getPins()>10 && !this.lastFrame){
            throw new InvalidRollException();
        }
        this.getRolls().add(roll);
    }

    public int getScore(){
        int pinScore =0;
        if(this.isLastFrame()){
            pinScore = this.getRolls().stream().mapToInt(Roll::getPins).sum();

            if (this.getRolls().size()==2 && this.getRolls().get(0).isStrike()){
                pinScore+=this.getRolls().get(1).getPins();
            }else if(this.getRolls().size() ==3 ){

            if(this.getRolls().get(0).isStrike()) {
                pinScore+=this.getRolls().get(1).getPins();
                pinScore+=this.getRolls().get(2).getPins();
            }
            }
        }else {
         pinScore = this.getRolls().stream().mapToInt(Roll::getPins).sum();
        if(nonNull(nextFrame)){
            if (this.getRolls().get(0).isStrike()){
                    // A strike is 10 points + number of pins on next 2 rolls
                    if (nextFrame.getRolls().get(0).isStrike()) {
                        pinScore +=10;
                        if (nextFrame.isLastFrame() && nextFrame.getRolls().size()>1){
                            pinScore =  nextFrame.getRolls().get(1).getPins();
                        }else {
                            if (nonNull(nextFrame.getNextFrame())) {
                                pinScore += nextFrame.getNextFrame().getRolls().get(0).getPins();
                            }
                        }
                    }else{
                        pinScore += nextFrame.getRolls().get(0).getPins();
                        if (nextFrame.getRolls().size()>1) {
                            pinScore += nextFrame.getRolls().get(1).getPins();
                        }
                    }
            } else if (this.isSpare()) {
                if (nextFrame.getRolls().size()>0){
                    pinScore += nextFrame.getRolls().get(0).getPins();
                }
            }
        }

    }
        return pinScore;
    }
}
