package be.thebeehive.kata.api.repositories;

import be.thebeehive.kata.api.models.BowlingGame;
import be.thebeehive.kata.api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameRepository extends JpaRepository<BowlingGame,Long> {

    BowlingGame save(BowlingGame b);
    BowlingGame getBowlingGameByGameId(String gameId);
    List<BowlingGame> getBowlingGamesByUser(User user);
    boolean existsBowlingGameByGameId(String gameId);

}
