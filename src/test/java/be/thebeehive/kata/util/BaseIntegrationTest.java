package be.thebeehive.kata.util;

import be.thebeehive.kata.api.configuration.UserDetailsImpl;
import be.thebeehive.kata.api.configurations.SpringSecurityTestConfig;
import be.thebeehive.kata.api.dto.BowlingGameDto;
import be.thebeehive.kata.api.dto.CreateGameDto;
import be.thebeehive.kata.api.dto.RollDto;
import be.thebeehive.kata.api.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest( classes = SpringSecurityTestConfig.class)
@AutoConfigureMockMvc
@AutoConfigureTestEntityManager
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)//Fresh H2
public abstract class BaseIntegrationTest {
    protected static final Faker FAKER = Faker.instance();
    @Autowired
    public TestEntityManager entityManager;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    public static User getUniqueDummyUser(){
        return new User("Valid", "dummy@dummy.be","Password10_");
    }
    @SneakyThrows
    protected String setupGame() {
        User dummy = getUniqueDummyUser();
        dummy = entityManager.persist(dummy);
        return deserialize(
                mockMvc.perform(
                                post("/bowling").with(user(new UserDetailsImpl(dummy)))
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(objectMapper.writeValueAsString(new CreateGameDto(FAKER.starTrek().character())))
                        ).andReturn()
                        .getResponse()
                        .getContentAsString(),
                BowlingGameDto.class
        ).gameId();
    }

    @SneakyThrows
    protected BowlingGameDto consecutiveRolls(String gameId, int... rolls) {
        BowlingGameDto dto = null;

        for (int roll : rolls) {
            dto = roll(gameId, roll);
        }

        return dto;
    }

    @SneakyThrows
    private BowlingGameDto roll(String gameId, int pins) {
        return deserialize(mockMvc.perform(
                                post(String.format("/bowling/%s", gameId))
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(serialize(new RollDto(pins)))
                        ).andReturn()
                        .getResponse()
                        .getContentAsString(),
                BowlingGameDto.class
        );
    }

    protected <T> String serialize(T object) throws Exception {
        return objectMapper.writeValueAsString(object);
    }

    protected <T> T deserialize(String json, Class<T> tClass) throws Exception {
        return objectMapper.readValue(json, tClass);
    }
}
