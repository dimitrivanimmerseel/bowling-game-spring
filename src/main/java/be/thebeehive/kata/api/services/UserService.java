package be.thebeehive.kata.api.services;

import be.thebeehive.kata.api.exceptions.EmailExistsException;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.api.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;


@Service
@Log4j2
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User createUser(User user){
        if (userRepository.existsByEmail(user.getEmail())){
            throw new EmailExistsException();
        }
        user = userRepository.save(user);
        return user;
    }

}
