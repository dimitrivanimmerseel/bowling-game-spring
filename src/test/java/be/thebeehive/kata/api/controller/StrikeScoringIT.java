package be.thebeehive.kata.api.controller;

import be.thebeehive.kata.util.BaseIntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
class StrikeScoringIT extends BaseIntegrationTest {
    private String gameId;

    @BeforeEach
    void setUp() {
        gameId = setupGame();
    }

    @ParameterizedTest
    @MethodSource("strikeSource")
    @Transactional
    void aStrikeDoublesTheNextTwoRolls(int[] rolls, int expectedScore) {
        assertThat(consecutiveRolls(gameId, rolls).score()).isEqualTo(expectedScore);
    }

    private static Stream<Arguments> strikeSource() {
        return Stream.of(
                Arguments.of(new int[]{10, 3, 5}, 26),
                Arguments.of(new int[]{10, 10, 3, 6}, 51),
                Arguments.of(new int[]{10, 10,10, 10,10, 10,10, 10,10, 10,10, 10}, 300)
        );
    }
}
