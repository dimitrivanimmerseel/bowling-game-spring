package be.thebeehive.kata.api.exceptions;

public class InvalidGameIdException extends RuntimeException {
    public InvalidGameIdException(String gameId) {
        super(gameId);
    }
}
