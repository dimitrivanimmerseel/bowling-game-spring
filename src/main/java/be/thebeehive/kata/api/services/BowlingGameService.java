package be.thebeehive.kata.api.services;

import be.thebeehive.kata.api.dto.BowlingGameDto;
import be.thebeehive.kata.api.dto.CreateGameDto;
import be.thebeehive.kata.api.dto.RollDto;
import be.thebeehive.kata.api.exceptions.InvalidGameIdException;
import be.thebeehive.kata.api.models.BowlingGame;
import be.thebeehive.kata.api.models.Roll;
import be.thebeehive.kata.api.models.User;
import be.thebeehive.kata.api.repositories.GameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;
@Service //Same as component, readability
@RequiredArgsConstructor
public class BowlingGameService {

    private final GameRepository gameRepository;
    public BowlingGameDto createGame(CreateGameDto createGameDto, User user) {
        //A new game gets a unique id, the given name and starts with score 0
        BowlingGame game = new BowlingGame(createGameDto.name());
        game.setUser(user);
        game = gameRepository.save(game);
        return new BowlingGameDto(game.getGameId(), game.getName(), 0);
    }

    public BowlingGameDto saveRoll(RollDto rollDto, String gameId) {
        BowlingGame game = getBowlingGameByGameId(gameId);
            Roll roll = new Roll(rollDto.pins());
            game.addScore(roll);

            game=gameRepository.save(game);
            return BowlingGameDto.builder()
                    .gameId(game.getGameId())
                    .name(game.getName())
                    .score(game.getScore()).build();

    }
    public BowlingGame getBowlingGameByGameId(String gameId){
        BowlingGame game =  gameRepository.getBowlingGameByGameId(gameId);
        if(isNull(game)){
            throw new InvalidGameIdException(gameId);
        }else{
            return game;
        }
    }
}
